#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "module/my_info.h"
char *s = NULL;
size_t len = 0;
const char banner_end[] = "---------------------------------------------\n\n";
const char prompt[] = "Which information do you want?\nVersion(v),CPU(c),Memory(m),Time(t),All(a),Exit(e)?\n";
FILE *fp;
char flag;
void output(const char * start, const char * end)
{
    while(getline(&s, &len, fp) != -1)
    {
        if(!strcmp(s, start))
        {
            flag = 1;
            continue;
        }
        else if(!strcmp(s, end) && flag)
        {
            flag = len = 0;
            free(s);
            s = NULL;
            break;
        }
        if(flag)
        {
            printf("%s", s);
        }
    }
    printf("%s", banner_end);
    return;
}

int main(void)
{
    fp = fopen("/proc/my_info", "r");
    printf(prompt);
    while(getline(&s, &len, stdin) != -1)
    {
        rewind(fp);
        printf("\n");
        switch(s[0])
        {
        case 'v':
            printf("Version: ");
            output(BANNER_V, BANNER_C);
            break;
        case 'c':
            printf("CPU information: \n");
            output(BANNER_C, BANNER_M);
            break;
        case 'm':
            printf("Memory information: \n");
            output(BANNER_M, BANNER_T);
            break;
        case 't':
            printf("Time information: \n");
            output(BANNER_T, "\n");
            break;
        case 'a':
            while(getline(&s, &len, fp) != -1)
            {
                printf("%s", s);
            }
            printf("%s", banner_end);
            free(s);
            s = NULL;
            len = 0;
            break;
        case 'e':
            free(s);
            fclose(fp);
            exit(0);
        }
        printf(prompt);
    }
    return 0;
}
