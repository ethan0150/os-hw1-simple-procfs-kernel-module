#ifdef MY_INFO
#define MY_INFO
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/version.h>
#include <asm/processor.h>
#include <linux/mm.h>
#include <linux/mmzone.h>
#include <linux/vmstat.h>
#include <linux/swap.h>
#include <asm/page.h>
#include <asm/pgtable.h>
#include <linux/sysinfo.h>
#include <generated/utsrelease.h>
#include <linux/time.h>
#include <linux/kernel_stat.h>
#include <linux/sched.h>
#endif /*MY_INFO*/

#define BANNER_V "====================Version====================\n"
#define BANNER_C "====================CPU====================\n"
#define BANNER_M "====================Memory====================\n"
#define BANNER_T "====================Time====================\n"

