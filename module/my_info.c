#define MY_INFO
#include "my_info.h"
#define procfs_name "my_info"

static struct proc_dir_entry *proc_file;

static int show_meminfo(struct seq_file *m)
{
    struct sysinfo i;
    unsigned long pages[NR_LRU_LISTS];
    int lru;
    si_meminfo(&i);

    for (lru = LRU_BASE; lru < NR_LRU_LISTS; lru++)
        pages[lru] = global_node_page_state(NR_LRU_BASE + lru);

    seq_printf(m, "MemTotal\t: %lu kB\n", i.totalram << (PAGE_SHIFT - 10));
    seq_printf(m, "MemFree\t\t: %lu kB\n", i.freeram << (PAGE_SHIFT - 10));
    seq_printf(m, "Buffers\t\t: %lu kB\n", i.bufferram << (PAGE_SHIFT - 10));
    seq_printf(m, "Active\t\t: %lu kB\n", (pages[LRU_ACTIVE_ANON] +
                                           pages[LRU_ACTIVE_FILE]) << (PAGE_SHIFT - 10));
    seq_printf(m, "Inactive\t: %lu kB\n", (pages[LRU_INACTIVE_ANON] +
                                           pages[LRU_INACTIVE_FILE]) << (PAGE_SHIFT - 10));
    seq_printf(m, "Shmem\t\t: %lu kB\n", i.sharedram << (PAGE_SHIFT - 10));
    seq_printf(m, "Dirty\t\t: %lu kB\n",
               global_node_page_state(NR_FILE_DIRTY) << (PAGE_SHIFT - 10));
    seq_printf(m, "Writeback\t: %lu kB\n",
               global_node_page_state(NR_WRITEBACK) << (PAGE_SHIFT - 10));
    seq_printf(m, "KernelStack\t: %lu kB\n",
               global_zone_page_state(NR_KERNEL_STACK_KB));
    seq_printf(m, "PageTables\t: %lu kB\n\n",
               global_zone_page_state(NR_PAGETABLE) << (PAGE_SHIFT - 10));

    return 0;
}
static int show_uptime(struct seq_file *m)
{
    struct timespec uptime;
    struct timespec idle;
    u64 nsec;
    u32 rem;
    int i;

    nsec = 0;
    for_each_possible_cpu(i)
    nsec += (__force u64) kcpustat_cpu(i).cpustat[CPUTIME_IDLE];

    get_monotonic_boottime(&uptime);
    idle.tv_sec = div_u64_rem(nsec, NSEC_PER_SEC, &rem);
    idle.tv_nsec = rem;
    seq_printf(m, "Uptime\t\t: %lu.%02lu (s)\nIdeltime\t: %lu.%02lu (s)\n\n",
               (unsigned long) uptime.tv_sec,
               (uptime.tv_nsec / (NSEC_PER_SEC / 100)),
               (unsigned long) idle.tv_sec,
               (idle.tv_nsec / (NSEC_PER_SEC / 100)));
    return 0;
}

static void *c_start(struct seq_file *m, loff_t *pos)
{
    *pos = cpumask_next(*pos - 1, cpu_online_mask);
    if ((*pos) == 0)
    {
        seq_printf(m, BANNER_V);
        seq_printf(m, "Linux version %s\n\n", UTS_RELEASE);
        seq_printf(m, BANNER_C);
    }
    if ((*pos) < nr_cpu_ids)
        return &cpu_data(*pos);
    else
    {
        seq_printf(m, BANNER_M);
        show_meminfo(m);
        seq_printf(m, BANNER_T);
        show_uptime(m);
    }
    return NULL;
}

static void *c_next(struct seq_file *m, void *v, loff_t *pos)
{
    (*pos)++;
    return c_start(m, pos);
}

static void c_stop(struct seq_file *m, void *v)
{
}

static int show_cpuinfo(struct seq_file *m, void *v)
{
    struct cpuinfo_x86 *c = v;
    unsigned int cpu;

    cpu = c->cpu_index;
    seq_printf(m, "processor\t: %u\n"
               "model name\t: %s\n",
               cpu,
               c->x86_model_id[0] ? c->x86_model_id : "unknown");
    seq_printf(m, "physical id\t: %d\n", c->phys_proc_id);
    seq_printf(m, "core id\t\t: %d\n", c->cpu_core_id);
    if (c->x86_cache_size >= 0)
        seq_printf(m, "cache size\t: %d KB\n", c->x86_cache_size);
    seq_printf(m, "clflush size\t: %u\n", c->x86_clflush_size);
    seq_printf(m, "cache_alignment\t: %d\n", c->x86_cache_alignment);
    seq_printf(m, "address sizes\t: %u bits physical, %u bits virtual\n",
               c->x86_phys_bits, c->x86_virt_bits);
    seq_puts(m, "\n");

    return 0;
}
const struct seq_operations cpuinfo_op =
{
    .start	= c_start,
    .next	= c_next,
    .stop	= c_stop,
    .show	= show_cpuinfo,
};
static int cpuinfo_open(struct inode *inode, struct file *file)
{
    return seq_open(file, &cpuinfo_op);
}

static const struct file_operations proc_cpuinfo_operations =
{
    .open		= cpuinfo_open,
    .read		= seq_read,
    .llseek		= seq_lseek,
    .release	= seq_release,
};

static int __init my_cpuinfo_init(void)
{
    proc_file = proc_create(procfs_name, 0, NULL, &proc_cpuinfo_operations);
    pr_info("/proc/%s created\n", procfs_name);
    return 0;
}
static void __exit my_cpuinfo_exit(void)
{
    proc_remove(proc_file);
    pr_info("/proc/%s removed\n", procfs_name);
}
module_init(my_cpuinfo_init);
module_exit(my_cpuinfo_exit);

MODULE_LICENSE("GPL");
